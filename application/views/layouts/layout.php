
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>oPEN Cart | Electronic Store</title
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>">    
    <?php render('css') ?>
    <link rel="stylesheet" href="<?= base_url('public/dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/skins/_all-skins.min.css') ?>">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="../../index2.html" class="logo">
          <span class="logo-mini"><b>o</b>C</span>
          <span class="logo-lg"><b>oPEN</b>Cart</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-usd"><span>  Currency    <i class="fa fa-caret-down"></i></span></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Money</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <i class="fa fa-euro"></i>
                          </div>
                          <h4>
                            Euro
                          </h4>
                        </a>
                        <a href="#">
                          <div class="pull-left">
                            <i class="fa fa-gbp"></i>
                          </div>
                          <h4>
                            Pond Sterling
                          </h4>
                        </a>
                        <a href="#">
                          <div class="pull-left">
                            <i class="fa fa-usd"></i>
                          </div>
                          <h4>
                            US Dollar
                          </h4>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-book"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Our Store Contact</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <i class="fa fa-phone"></i>
                          </div>
                          <h4>
                            123456789
                          </h4>
                          <p>This is our store phone number</p>
                        </a>
                        <a href="#">
                          <div class="pull-left">
                            <i class="fa fa-mobile"></i>
                          </div>
                          <h4>
                            987654321
                          </h4>
                          <p>This is our store mobile number</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">My Account</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-user-plus"></i> Register
                        </a>
                        <a href="#">
                          <i class="fa fa-sign-in"></i> Log In
                        </a>
                        <a href="#">
                          <i class="fa fa-sign-out"></i> Log Out
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>              
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-fa fa-heart"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Your wishlist</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-ship"></i>     
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-shopping-cart"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Your Shopping Cart</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart"></i>     
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-share-square-o"></i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <?php $this->load->view('layouts/partials/sidemenu') ?>
        </section>
      </aside>
      <div class="content-wrapper">
        <section class="content">
          <?php render('content') ?>
        </section>
      </div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>
    </div>    
    <script src="<?= base_url('public/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/fastclick/fastclick.min.js') ?>"></script>
    <script src="<?= base_url('public/dist/js/app.min.js') ?>"></script>
  </body>
</html>