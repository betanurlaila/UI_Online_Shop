<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>oPEN Cart | Electronics Store</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  </head>
<ul class="sidebar-menu">
  <li class="header">OUR MENU</li>
  <li>
    <a href="#">
      <i class="fa fa-home"></i> <span>Home</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-desktop"></i> <span>Desktops</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa fa-circle-o"></i> PC (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Mac (1)</a></li>
      <li><a href="#">See All Desktops</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i><span>Laptops & Notebooks</span><i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa fa-circle-o"></i> Macs (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Windows (0)</a></li>
      <li><a href="#">See All Laptops & Notebooks</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-print"></i><span>Components</span><i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa fa-circle-o"></i> Mice and Trackballs (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Monitors (2)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Printers (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Scanners (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Web Cameras (0)</a></li>
      <li><a href="#">See All Components</a></li>
    </ul>
  </li>
  <li>
    <a href="#">
      <i class="fa fa-tablet"></i> <span>Tablets</span>
    </a>
  </li>
  <li>
    <a href="#">
      <i class="fa fa-windows"></i> <span>Software</span>
    </a>
  </li>
  <li>
    <a href="#">
      <i class="fa fa-mobile"></i> <span>Phones & PDAs</span>
    </a>
  </li>
  <li>
    <a href="#">
      <i class="fa fa-camera"></i> <span>Cameras</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-headphones"></i><span>MP3 Players</span><i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa fa-circle-o"></i> Test 1 (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Test 2 (0)</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Test 3 (0)</a></li>
      <li><a href="#">See All MP3 Players</a></li>
    </ul>
  </li>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="plugins/knob/jquery.knob.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/pages/dashboard.js"></script>
    <script src="dist/js/demo.js"></script>
  </body>
</html>