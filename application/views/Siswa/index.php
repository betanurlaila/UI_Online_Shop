<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>

<?php section('content') ?>

<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Data Siswa</h3>
  </div>
  <a href="<?= base_url('siswa/create')?>">create</a>

  <div class="box-body">
    <table class="table table-bordered table-condensed">
      <thead>
        <thead>
          <tr>
            <th>Nama</th>
            <th>Kelas</th>
            <th></th>
          </tr>
          <td></td>
          <td></td>
   <td><a href="<?= base_url('siswa/edit')?>">edit</a></td>
        </thead>
      </thead>
    </table>
  </div><!-- /.box-body -->
</div><!-- /.box -->
<?php endsection() ?>

<?php getview('layouts/layout') ?>